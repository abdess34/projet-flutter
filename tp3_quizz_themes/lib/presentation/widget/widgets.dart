import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tp3_quizz_themes/buisness_logic/bloc/question_bloc.dart';
import 'package:tp3_quizz_themes/buisness_logic/cubit/answer_question.dart';
import 'package:tp3_quizz_themes/buisness_logic/cubit/next_question.dart';
import 'package:tp3_quizz_themes/buisness_logic/cubit/score_quizz.dart';
import 'package:tp3_quizz_themes/data/models/Thematique.dart';
import 'package:tp3_quizz_themes/data/repository/thematique_repo.dart';
import 'package:tp3_quizz_themes/presentation/pages/accueil.dart';
import 'package:tp3_quizz_themes/data/repository/question_repo.dart';

import '../../buisness_logic/bloc/thematique_bloc.dart';
import '../pages/formulaire_question.dart';
import '../pages/quizz_page.dart';
import '../pages/update_question.dart';

class ImageQuiz extends StatelessWidget {
  const ImageQuiz({
    Key? key,
    required this.url,
  }) : super(key: key);

  final String url;

  @override
  Widget build(BuildContext context) {
    return Container(
      // Container for Image
      width: 350,
      height: 200,
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.circular(10),
        image: DecorationImage(image: NetworkImage(url), fit: BoxFit.cover),
      ),
    );
  }
}



class IndexQuiz extends StatelessWidget {
  const IndexQuiz({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CubitNextQuestion, int>(
      builder: (_, index) {
        return Center(
          child: BlocBuilder<QuestionBloc, QuestionState>(
            builder: (context, state) {
              if (state is QuestionLoaded) {
                return Text(
                    'Question ${index + 1}/${state.questions.length}',
                    style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold));
              } else {
                return Container();
              }
            },
          ),
        );
      },
    );
  }
}
class NoQuestionContainer extends StatelessWidget {
  const NoQuestionContainer({
    Key? key,
    required this.thematique,
  }) : super(key: key);

  final Thematique thematique;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 50,
              ),
              const Text("Il n'y a pas encore de questions dans ce quiz."),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    // on va sur le formulaire ajout de question
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FormulaireQuestionsPage(
                            thematique: thematique.nom,
                          )),
                    );
                  },
                  child: const Text("Ajouter une question")),
            ]));
  }
}
class ScoreQuiz extends StatelessWidget {
  const ScoreQuiz({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder< CubitScoreQuiz, int>(
      builder: (_, score) {
        return Center(
            child: Text('Score : $score',
                style: const TextStyle(fontSize: 18)));
      },
    );
  }
}
class ButtonDelete extends StatelessWidget {
  const ButtonDelete({
    Key? key,
    required this.thematique,
  }) : super(key: key);

  final String thematique;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(right: 20.0),
        child: GestureDetector(
          onTap: () => showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: Text('Supression du thème : $thematique'),
              content: Text("Voulez vous supprimer le thème $thematique ?"),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context, 'Non'),
                  child: const Text('Non'),
                ),

                TextButton(
                  onPressed: () {
                    final ThemeRepository repository = ThemeRepository();
                    repository.deleteTheme(thematique);

                    // on retourne à la page home
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const Accueil(
                            title: 'Thématiques',
                          )),
                    );
                  },
                  child: const Text('Oui'),
                ),
              ],
            ),
          ),
          child: const Icon(
            Icons.delete,
            size: 26.0,
          ),
        ));
  }
}
class ButtonsQuiz extends StatelessWidget {
  const ButtonsQuiz({
    Key? key,
    required this.state,
  }) : super(key: key);

  final QuestionLoaded state;

  void resetCubit(BuildContext c) {
    c.read<CubitNextQuestion>().reset();
    c.read<CubitScoreQuiz>().reset();
    c.read<CubitAnswerQuestion>().reset();
  }

  void nextQuestion(BuildContext c, int index, int indexMax) {
    if (index + 1 == indexMax) {
      // Restart or return to menu
      //showFinishDialog(c);
      resetCubit(c);
    } else {
      // Next Question
      c.read<CubitNextQuestion>().next();
      c.read<CubitAnswerQuestion>().reset();
    }
  }

  void checkAnswer(BuildContext c, bool answer, bool userAnswer) {
    if (answer == userAnswer) {
      c.read<CubitAnswerQuestion>().correct();
      c.read<CubitScoreQuiz>().goodRep();
    } else {
      c.read<CubitAnswerQuestion>().incorrect();
      c.read<CubitScoreQuiz>().badRep();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      // Container for Buttons
        width: 350,
        child: BlocBuilder<CubitNextQuestion, int>(builder: (_, index) {
          return BlocBuilder<CubitAnswerQuestion, int>(builder: (_, answer) {
            return Column(
              children: [
                if (answer == 2) ...[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: 100,
                        child: ElevatedButton(
                          onPressed: answer != 2
                              ? null
                              : () => checkAnswer(
                              context,
                              state.questions.elementAt(index)!.isCorrect,
                              true),
                          child: const Text("Vrai",
                              style: TextStyle(fontSize: 18)),
                        ),
                      ),
                      SizedBox(
                        width: 100,
                        child: ElevatedButton(
                          // si c'est la fin du quiz on reset
                          onPressed: answer != 2
                              ? null
                              : () => checkAnswer(
                              context,
                              state.questions.elementAt(index)!.isCorrect,
                              false),
                          child: const Text("Faux",
                              style: TextStyle(fontSize: 18)),
                        ),
                      ),
                    ],
                  ),
                ],
                SizedBox(
                  width: 350,
                  child: (state.questions.length-1 == index) && (answer != 2)
                      ? ElevatedButton(
                    // Retour au menu
                    onPressed: () {
                      resetCubit(context);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const Accueil(
                              title: 'Thématiques',
                            )),
                      );
                    },
                    child: const Text("Retour au menu", style: TextStyle(fontSize: 18)),
                  )
                      : null,
                ),
                const SizedBox(height: 15),
                SizedBox(
                  width: 350,
                  child: answer == 2
                      ? null
                      : ElevatedButton(
                    // si c'est la fin du quiz on reset
                    onPressed: (answer == 2
                        ? null
                        : () => nextQuestion(
                        context, index, state.questions.length)),
                    child:
                    Text(
                        state.questions.length-1 != index
                            ? "Suivant"
                            : "Recommencer",
                        style: const TextStyle(fontSize: 18)),

                  ),
                )
              ],
            );
          });
        }));
  }
}
class QuestionItemContainer extends StatelessWidget {
  const QuestionItemContainer({
    Key? key,
    required this.data,
    required this.id,
  }) : super(key: key);

  final String id;
  final Map<String, dynamic> data;

  @override
  Widget build(BuildContext context) {
    QuestionRepository repository = QuestionRepository();

    return Row(
      //mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            width: 320,
            margin: const EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              color: Theme.of(context).cardColor,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 240,
                      padding: const EdgeInsets.all(10.0),
                      child: Text(data["intitule"],
                          style: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context).hintColor,
                              fontWeight: FontWeight.bold)),
                    ),
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(data["isCorrect"] ? "Vrai" : "Faux",
                          style: TextStyle(
                              fontSize: 14,
                              color: Theme.of(context).hintColor,
                              fontWeight: FontWeight.bold)),
                    ),

                  ],
                )
              ],
            )),

      ],
    );
  }
}

class ThematiqueItemContainer extends StatelessWidget {
  const ThematiqueItemContainer({
    Key? key,
    required this.state,
    required this.index,
  }) : super(key: key);

  final ThemeLoaded state;
  final int index;

  @override
  Widget build(BuildContext context) {
    void resetCubit(BuildContext c) {
      c.read<CubitNextQuestion>().reset();
      c.read<CubitScoreQuiz>().reset();
      c.read<CubitAnswerQuestion>().reset();
    }

    return InkWell(

      // bouton sur le container
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => QuizPage(
                thematique: state.getThemes.elementAt(index) as Thematique,
              )),
        ).then((value) {
          resetCubit(context); // On reset les cubit lorsque l'on va sur un quiz
        });
      },
      child: Container(
          width: 200,
          margin: const EdgeInsets.all(10.0),
          decoration: BoxDecoration(
            color: Theme.of(context).cardColor,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.vertical(
                        top: Radius.circular(10), bottom: Radius.circular(0)),
                    image: DecorationImage(
                        image: NetworkImage(
                            (state.getThemes.elementAt(index)!.getUrl())),
                        fit: BoxFit.cover)),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10.0),
                    child: Text(state.getThemes.elementAt(index)!.nom,
                        style: TextStyle(
                            fontSize: 18,
                            color: Theme.of(context).hintColor,
                            fontWeight: FontWeight.bold)),
                  ),
                  IconButton(
                      icon: const Icon(
                        Icons.settings,
                      ),
                      color: Theme.of(context).hintColor,
                      onPressed: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => UpdateQuestionsPage(
                              thematique:
                              state.getThemes.elementAt(index)!.nom,
                            )),
                      ))
                ],
              )
            ],
          )),
    );
  }
}