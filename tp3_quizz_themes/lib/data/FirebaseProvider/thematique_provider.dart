import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:tp3_quizz_themes/data/models/Question.dart';
import 'package:tp3_quizz_themes/data/models/Thematique.dart';

final FirebaseFirestore firestore = FirebaseFirestore.instance;


class ThemeProvider {

  // --- COLLECTION REFERENCE ---
  static CollectionReference getGroupCollection() {
    return firestore.collection('thematiques');
  }

  // --- GET ALL ---
  Query getAllTheme() {
    return ThemeProvider.getGroupCollection().withConverter<Thematique>(
      fromFirestore: (snapshot, _) => Thematique.fromJson(snapshot.data()!),
      toFirestore: (nom, _) => nom.toJson(),
    );
  }

  // --- GET ---
  Future<DocumentSnapshot<Object?>> getTheme(String id) {
    return ThemeProvider.getGroupCollection().doc(id).get();
  }

  // --- ADD ---
  Future<void> addTheme(Thematique newTheme) async {
    return ThemeProvider.getGroupCollection()
        .doc(newTheme.nom)
        .set(newTheme.toJson());
  }

  // --- DELETE ---
  Future<void> deleteTheme(String id) async {
    return ThemeProvider.getGroupCollection().doc(id).delete();
  }
}