import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tp3_quizz_themes/data/FirebaseProvider/question_provider.dart';
import 'package:tp3_quizz_themes/data/models/Question.dart';


class QuestionRepository {
  QuestionRepository({Key? key});
  final _questionProvider = QuestionProvider();

  Stream<QuerySnapshot> getAllQuestions() =>
      _questionProvider.getAllQuestions().snapshots();


  Stream<QuerySnapshot> getAllQuestionsByThematique(String thematique) =>
      _questionProvider.getAllQuestionsByThematique(thematique).snapshots();


  Future<List<Question?>> getQuestionsList() async {
    return _questionProvider.getAllQuestions().get().then((snapshot){
      final List<Question?> questions = [];
      for (var doc in snapshot.docs) {
        questions.add(doc.data() as Question);
      }
      return questions;
    });
  }

  Future<List<Question?>> getQuestionsThematiqueList(String thematique) async {
    return _questionProvider.getAllQuestionsByThematique(thematique).get().then((snapshot){
      final List<Question?> questions = [];
      for (var doc in snapshot.docs) {
        questions.add(doc.data() as Question);
      }
      return questions;
    });
  }



  Future<DocumentSnapshot> getQuestion(String id) async =>
      await _questionProvider.getQuestion(id);

  Future<void> addQuestion(String intitule, bool isCorrect, String thematique) async =>
      await _questionProvider.addQuestion(Question(intitule: intitule, isCorrect: isCorrect, thematique: thematique));

  Future<void> deleteQuestion(String id) async =>
      await _questionProvider.deleteQuestion(id);


}