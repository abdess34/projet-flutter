import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:tp3_quizz_themes/data/FirebaseProvider/thematique_provider.dart';
import 'package:tp3_quizz_themes/data/models/Question.dart';
import 'package:tp3_quizz_themes/data/models/Thematique.dart';


class ThemeRepository {
  ThemeRepository({Key? key});
  final _themeProvider = ThemeProvider();

  Stream<QuerySnapshot> getAllTheme() =>
      _themeProvider.getAllTheme().snapshots();


  Future<List<Thematique?>> getThemeList() async {
    return _themeProvider.getAllTheme().get().then((snapshot){
      final List<Thematique?> themes = [];
      for (var doc in snapshot.docs) {
        themes.add(doc.data() as Thematique);
      }
      return themes;
    });
  }


  Future<DocumentSnapshot> getTheme(String id) async =>
      await _themeProvider.getTheme(id);

  Future<void> addTheme(String nom, String url) async =>
      await _themeProvider.addTheme(Thematique(nom: nom, url: url));

  Future<void> deleteTheme(String id) async =>
      await _themeProvider.deleteTheme(id);


}