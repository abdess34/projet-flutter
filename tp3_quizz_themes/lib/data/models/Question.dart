class Question {
  final String intitule;
  final String thematique;
  final bool isCorrect;

  Question({required this.intitule, required this.isCorrect, required this.thematique,});


  bool valid(bool resp) {
    return resp == isCorrect;
  }

  Map<String, dynamic> toJson() => _questionToJson(this);

  Question.fromJson(Map<String, dynamic> json)
      : this(
    intitule: json["intitule"] as String,
    isCorrect: json["isCorrect"] as bool,
    thematique: json["thematique"] as String,
  );

  @override
  String toString() => "Question: $intitule -> ($isCorrect) : $thematique";

  Map<String, dynamic> _questionToJson(Question instance) => <String, dynamic>{
    'intitule': instance.intitule,
    'isCorrect': instance.isCorrect,
    'thematique' : instance.thematique
  };
}