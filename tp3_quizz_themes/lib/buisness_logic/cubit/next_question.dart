import 'package:flutter_bloc/flutter_bloc.dart';

class CubitNextQuestion extends Cubit<int>{
  CubitNextQuestion() : super(0);
  void next() => emit(state+1);
  void reset() => emit(0);
}