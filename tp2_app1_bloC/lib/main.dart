import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tp2_app1_bloc/cubit/next_question.dart';
import 'package:tp2_app1_bloc/cubit/score_quizz.dart';
import 'package:tp2_app1_bloc/cubit/answer_question.dart';
import 'package:tp2_app1_bloc/vues/quizz_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<CubitNextQuestion>(
          create: (BuildContext context) => CubitNextQuestion(),
        ),
        BlocProvider<CubitScoreQuiz>(
          create: (BuildContext context) => CubitScoreQuiz(),
        ),
        BlocProvider<CubitAnswerQuestion>(
          create: (BuildContext context) => CubitAnswerQuestion(),
        ),
      ],
      child: MaterialApp(
        title: 'Questions/Réponses',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyQuizPage(title: 'Questions/Réponses'),
      ),
    );
  }
}

