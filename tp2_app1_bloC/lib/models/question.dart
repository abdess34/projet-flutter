class Question{
  String questionText;
  bool isCorrect;

  Question({required this.questionText, required this.isCorrect});

  bool valid(bool resp){
    return resp == isCorrect;
  }

}