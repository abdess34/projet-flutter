import 'package:flutter/cupertino.dart';
import 'package:tp2_app1_bloc/models/question.dart';
class MyQuizz extends ChangeNotifier{
  final List<Question> _questionsQuizz = [
    Question(questionText: "Il n’existe plus aucun marsupial à l’état sauvage en Amérique du Nord.", isCorrect: false),
    Question(questionText: "Le monito del monte est le plus petit marsupial d’Australie.", isCorrect: false),
    Question(questionText: "La queue du dunnart à pieds étroits lui permet de stocker le surplus de graisse.",isCorrect: true),
    Question(questionText: "Le temps de gestation chez certaines espèces de bandicoots est de seulement 12 jours..",isCorrect: true),
    Question(questionText: "Les femelles du diable de Tasmanie dégagent une forte odeur pour attirer les mâles lors de la saison de l’amour.",isCorrect: false),
    Question(questionText: "Comme chez les rongeurs, les incisives du wombat n’arrêtent jamais de pousser.",isCorrect: true),
    Question(questionText: "Comme les vraies taupes, les taupes marsupiales laissent derrière eux de longs tunnels sous le sol.", isCorrect:false),
    Question(questionText: "Le quokka peut vivre sans boire d’eau pendant environ un mois.",isCorrect: true),
    Question(questionText: "Avec ses griffes puissantes, le numbat est capable d’éventrer les termitières.",isCorrect: false),
    Question(questionText: "Le dasyure est majoritairement carnivore.",isCorrect: true),
  ];

  MyQuizz();

  String getQuestion(int i){
    return _questionsQuizz[i].questionText.toString();
  }
  bool questionValidation(int i, bool rep){
    return _questionsQuizz[i].isCorrect == rep;
  }

  int getNbQuestions(){
    return _questionsQuizz.length;
  }




}