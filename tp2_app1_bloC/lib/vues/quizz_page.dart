import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:tp2_app1_bloc/models/quizz_game.dart';
import 'package:tp2_app1_bloc/cubit/answer_question.dart';
import 'package:tp2_app1_bloc/cubit/next_question.dart';
import 'package:tp2_app1_bloc/cubit/score_quizz.dart';

class MyQuizPage extends StatelessWidget {
  MyQuizPage({Key? key, required this.title}) : super(key: key);


  final game = MyQuizz();

  final String title;

  @override
  Widget build(BuildContext context) {
    var _resultQuestion = 2;

    void nextQuestion(BuildContext c, int index) {
      if (index + 1 == game.getNbQuestions()) { //derniere question
        c.read<CubitNextQuestion>().reset();
        c.read<CubitScoreQuiz>().reset();
      } else {
        c.read<CubitNextQuestion>().next();
      }
      c.read<CubitAnswerQuestion>().reset();
    }

    void checkAnswer(BuildContext c, int index, bool answer) {
      if (game.questionValidation(index, answer)) {
        c.read<CubitAnswerQuestion>().correct();
        c.read<CubitScoreQuiz>().goodRep();
      } else {
        c.read<CubitAnswerQuestion>().incorrect();
        c.read<CubitScoreQuiz>().badRep();
      }
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const SizedBox(height: 20),
            Container(
              // Container for Header (score and index)
                width: 350,
                height: 50,
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    BlocBuilder<CubitNextQuestion, int>(
                      builder: (_, index) {
                        return Center(
                          child: Text(
                              'Question ${index + 1}/${game.getNbQuestions()}',
                              style: const TextStyle(
                                  fontSize: 18,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                        );
                      },
                    ),
                    BlocBuilder<CubitScoreQuiz, int>(
                      builder: (_, score) {
                        return Center(
                            child: Text('Score : $score',
                                style: const TextStyle(fontSize: 18)));
                      },
                    ),
                  ],
                )),
            const SizedBox(height: 10),
            Container(
              // Container for Image
                width: 350,
                height: 200,
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: const Image(
                  image: NetworkImage(
                      "https://quipoquiz.com/sn_uploads/quizzes/terrestrial_FR.jpg"),
                )),
            const SizedBox(height: 20),
            BlocBuilder<CubitNextQuestion, int>(
              builder: (_, index) {
                return BlocBuilder<CubitAnswerQuestion, int>(
                    builder: (_, answer) {
                      return Container(
                        // Container for Questions
                          width: 350,
                          height: 150,
                          padding: const EdgeInsets.all(10),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Colors.transparent,
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                width: 2.0,
                                color: answer == 2
                                    ? Colors.blueGrey.shade200
                                    : (answer == 1 ? Colors.green : Colors.red),
                              )),
                          child: Text(
                            game.getQuestion(index),
                            style: const TextStyle(fontSize: 18),
                            textAlign: TextAlign.center,
                          ));
                    });
              },
            ),
            const SizedBox(height: 40),
            Container(
              // Container for Buttons
                width: 350,
                height: 50,
                child: BlocBuilder<CubitNextQuestion, int>(builder: (_, index) {
                  return BlocBuilder<CubitAnswerQuestion, int>(
                      builder: (_, answer) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ElevatedButton(
                              onPressed: answer != 2
                                  ? null
                                  : () => checkAnswer(context, index, true),
                              child: const Text("Vrai",
                                  style: TextStyle(fontSize: 18)),
                            ),
                            ElevatedButton(
                              // si c'est la fin du quiz on reset
                              onPressed: answer != 2
                                  ? null
                                  : () => checkAnswer(context, index, false),
                              child: const Text("Faux",
                                  style: TextStyle(fontSize: 18)),
                            ),
                            ElevatedButton(
                              // si c'est la fin du quiz on reset
                              onPressed: answer == 2
                                  ? null
                                  : () => nextQuestion(context, index),
                              child: Wrap(
                                children: [
                                  Text(
                                      index + 1 == game.getNbQuestions()
                                          ? "Recommencer"
                                          : "Suivant",
                                      style: const TextStyle(fontSize: 18)),
                                  const Icon(Icons.keyboard_arrow_right)
                                ],
                              ),
                            )
                          ],
                        );
                      });
                })),
          ],
        ),
      ),
    );
  }
}