import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:tp2_app1_provider/models/quizz_game.dart';
import 'package:tp2_app1_provider/vues/quizz_page.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => MyQuizz(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      title: 'Questions/Réponses',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: QuizPage(title: 'Questions/Réponses'),
    );

  }
}