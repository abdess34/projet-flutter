import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tp2_app1_provider/models/quizz_game.dart';

class QuizPage extends StatelessWidget {
  QuizPage({Key? key, required this.title}) : super(key: key);

  final String title;

  void _nextQuestion(BuildContext context){
    Provider.of<MyQuizz>(context, listen: false).nextQuestion();
  }

  void _checkAnswer(BuildContext context, bool answer){
    Provider.of<MyQuizz>(context, listen: false).checkAnswer(answer);
  }



  @override
  Widget build(BuildContext context){

    var _question = Provider.of<MyQuizz>(context).getQuestion();
    var _nbQuestion = Provider.of<MyQuizz>(context).getMaxQuestion();
    var _resultQuestion = Provider.of<MyQuizz>(context).getResult();
    var _isButtonDisabled = Provider.of<MyQuizz>(context).getButtonDisabled();
    var _score = Provider.of<MyQuizz>(context).getScore();
    var _index = Provider.of<MyQuizz>(context).getIndex();
    var _quizIsOver = Provider.of<MyQuizz>(context).quizIsOver();

    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(

          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const SizedBox(height: 20),
            Container( // Container for Header (score and index)
                width:350,
                height:50,
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Question ${_index+1}/$_nbQuestion',
                        style: const TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold)),
                    Text('Score : $_score',
                        style: const TextStyle(fontSize: 18))
                    ,],
                )
            ),
            const SizedBox(height: 10),
            Container( // Container for Image
                width:350,
                height:200,
                decoration: BoxDecoration(
                  color:  Colors.black12,
                  borderRadius: BorderRadius.circular(10),
                ),
                child: const Image(image: NetworkImage("https://quipoquiz.com/sn_uploads/quizzes/terrestrial_FR.jpg"),)
            ),
            const SizedBox(height: 20),
            Container( // Container for Questions
                width:350,
                height:150,
                padding: const EdgeInsets.all(10),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      width: 2.0,
                      color: _resultQuestion == 3 ? Colors.blueGrey.shade200 : (_resultQuestion == 1 ? Colors.green : Colors.red), )
                ),
                child:
                Text(_question,
                  style: const TextStyle(fontSize: 18),
                  textAlign: TextAlign.center,)
            ),
            const SizedBox(height: 40),
            Container( // Container for Buttons
              width:350,
              height:50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(
                    onPressed: _isButtonDisabled ? null : () => _checkAnswer(context, true),
                    child: const Text("Vrai", style: TextStyle(fontSize: 18)),
                  ),
                  ElevatedButton(
                      onPressed: _isButtonDisabled ? null : () => _checkAnswer(context, false),
                      child: const Text("Faux", style: TextStyle(fontSize: 18))
                  ),
                  ElevatedButton(
                    onPressed: !_isButtonDisabled ? null : () => _nextQuestion(context),
                    child: Wrap(children:  [
                      Text(_quizIsOver ? "Recommencer" :"Suivant", style: const TextStyle(fontSize: 18)),
                      const Icon(Icons.keyboard_arrow_right)
                    ],
                    ),

                  ),

                ],),
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),

    );
  }

}