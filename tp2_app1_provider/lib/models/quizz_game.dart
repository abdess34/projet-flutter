import 'package:flutter/cupertino.dart';
import 'package:tp2_app1_provider/models/Questions.dart';
class MyQuizz extends ChangeNotifier{
  int score = 0;
  int _index = 0; // emplacement de la question courante

  int _resp_correct = 3; // 3 états : aucune réponse = 3, vrai = 1, faux = 0
  bool _isButtonDisabled = false;

  final List<Question> _questions = [
    Question(questionText: "Il n’existe plus aucun marsupial à l’état sauvage en Amérique du Nord.", isCorrect: false),
    Question(questionText: "Le monito del monte est le plus petit marsupial d’Australie.", isCorrect: false),
    Question(questionText: "La queue du dunnart à pieds étroits lui permet de stocker le surplus de graisse.",isCorrect: true),
    Question(questionText: "Le temps de gestation chez certaines espèces de bandicoots est de seulement 12 jours..",isCorrect: true),
    Question(questionText: "Les femelles du diable de Tasmanie dégagent une forte odeur pour attirer les mâles lors de la saison de l’amour.",isCorrect: false),
    Question(questionText: "Comme chez les rongeurs, les incisives du wombat n’arrêtent jamais de pousser.",isCorrect: true),
    Question(questionText: "Comme les vraies taupes, les taupes marsupiales laissent derrière eux de longs tunnels sous le sol.", isCorrect:false),
    Question(questionText: "Le quokka peut vivre sans boire d’eau pendant environ un mois.",isCorrect: true),
    Question(questionText: "Avec ses griffes puissantes, le numbat est capable d’éventrer les termitières.",isCorrect: false),
    Question(questionText: "Le dasyure est majoritairement carnivore.",isCorrect: true),
  ];

  MyQuizz();

  String getQuestion(){
    return _questions[_index].questionText.toString();
  }

  String getMaxQuestion(){
    return _questions.length.toString();
  }

  int getResult(){
    return _resp_correct;
  }

  int getScore(){
    return score;
  }

  int getIndex(){
    return _index;
  }

  bool getButtonDisabled(){
    return _isButtonDisabled;
  }

  void nextQuestion() {
    // on passe au niveau suivant seulement si on a répondu à la question
    if(_isButtonDisabled){
      _index+1 == _questions.length ? reloadQuiz() : _index++;
      _resp_correct = 3;
      _isButtonDisabled = false;
      notifyListeners();
    }
  }

  void reloadQuiz() {
    _index = 0;
    score = 0;
    _resp_correct = 3;
    _isButtonDisabled = false;
    notifyListeners();
  }

  bool quizIsOver(){
    return (_index+1 == _questions.length) && _isButtonDisabled;
  }



  void checkAnswer(bool answer){
    if(answer == _questions[_index].isCorrect){
      _resp_correct = 1;
      score += 10;
      _isButtonDisabled = true;
    }else{
      _resp_correct = 0;
      score = (score > 0 ? score-5 : 0);
      _isButtonDisabled = true;
    }
    notifyListeners();
  }




}