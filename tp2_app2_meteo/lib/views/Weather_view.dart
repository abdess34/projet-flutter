import 'package:tp2_app2_meteo/services/network.dart';
import 'package:tp2_app2_meteo/models/weather_model.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WeatherViewModelCubit extends Cubit<WeatherModel>{
  WeatherModel weatherModel = WeatherModel();

  WeatherViewModelCubit() : super(WeatherModel());

  WeatherModel doSearch (String cityName){
    Network.fetchWeather(cityName: cityName).then((value) {
      weatherModel = value;
    }).catchError(onError)
        .whenComplete(() {

    });
    emit(weatherModel);
    return weatherModel;
  }

}