import 'package:tp2_app2_meteo/services/client.dart';
import 'package:tp2_app2_meteo/models/weather_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class Network{
  static Future<WeatherModel> fetchWeather({required String cityName}) async {
    final url = Uri.parse(Client.baseURL+ "$cityName" + Client.AppId);
    final response = await http.get(url);
    print(response.statusCode);
    if (response.statusCode == 200) {
      print(response.body);
      return WeatherModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load weather: $cityName');
    }
  }

}