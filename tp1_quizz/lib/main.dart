//import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Questions/Réponses',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const QuizPage(title: 'Questions/Réponses'),
    );
  }
}

class QuizPage extends StatefulWidget {
  const QuizPage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<QuizPage> createState() => SomeQuizzPageState();
}

class SomeQuizzPageState extends State<QuizPage> {
  int _score = 0;
  int _index = 0; // emplacement de la question courante
  int _resp_correct = 0; // 3 états : aucune réponse = 0, vrai = 1, faux = 3
  bool _isButtonDisabled = false;

  final List<Question> _questions = [
    Question(questionText: "Il n’existe plus aucun marsupial à l’état sauvage en Amérique du Nord.", isCorrect: false),
    Question(questionText: "Le monito del monte est le plus petit marsupial d’Australie.", isCorrect: false),
    Question(questionText: "La queue du dunnart à pieds étroits lui permet de stocker le surplus de graisse.",isCorrect: true),
    Question(questionText: "Le temps de gestation chez certaines espèces de bandicoots est de seulement 12 jours..",isCorrect: true),
    Question(questionText: "Les femelles du diable de Tasmanie dégagent une forte odeur pour attirer les mâles lors de la saison de l’amour.",isCorrect: false),
    Question(questionText: "Comme chez les rongeurs, les incisives du wombat n’arrêtent jamais de pousser.",isCorrect: true),
    Question(questionText: "Comme les vraies taupes, les taupes marsupiales laissent derrière eux de longs tunnels sous le sol.", isCorrect:false),
    Question(questionText: "Le quokka peut vivre sans boire d’eau pendant environ un mois.",isCorrect: true),
    Question(questionText: "Avec ses griffes puissantes, le numbat est capable d’éventrer les termitières.",isCorrect: false),
    Question(questionText: "Le dasyure est majoritairement carnivore.",isCorrect: true),
  ];

  void _nextQuestion() {
    setState(() {
      // on passe au niveau suivant seulement si on a répondu à la question
      if(_isButtonDisabled){
        _index+1 == _questions.length ? _reloadQuiz() : _index++;
        _resp_correct = 0;
        _isButtonDisabled = false;
      }
    });
  }

  void _reloadQuiz() {
    setState(() {
      _index = 0;
      _score = 0;
      _resp_correct = 0;
      _isButtonDisabled = false;
    });
  }

  bool _quizIsOver(){
    return (_index+1 == _questions.length) && _isButtonDisabled;

  }

  void _checkAnswerTrue(){
    if(true == _questions[_index].isCorrect){
      _isValid();
    }else{
      _isInvalid();
    }
  }

  void _checkAnswerFalse(){
    if(false == _questions[_index].isCorrect){
      _isValid();
    }else{
      _isInvalid();
    }
  }

  void _isInvalid(){
    setState(() {
      _resp_correct = 2;
      _score -= 5;
      _isButtonDisabled = true;
    });
  }

  void _isValid(){
    setState(() {
      _resp_correct = 1;
      _score += 10;
      _isButtonDisabled = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(

          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const SizedBox(height: 20),
            Container( // Container for Header (score and index)
                width:350,
                height:50,
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text('Question ${_index+1}/${_questions.length}',
                        style: const TextStyle(
                            fontSize: 18,
                            color: Colors.black,
                            fontWeight: FontWeight.bold)),
                    Text('Score : $_score',
                        style: const TextStyle(fontSize: 18))
                    ,],
                )
            ),
            const SizedBox(height: 10),
            Container( // Container for Image
                width:350,
                height:200,
                decoration: BoxDecoration(
                  color: Colors.black12,
                  borderRadius: BorderRadius.circular(10),
                ),
                //child: Image(image: NetworkImage("https://images.assetsdelivery.com/compings_v2/soifer/soifer1809/soifer180900063.jpg"),)
                child: Image(image: NetworkImage("https://quipoquiz.com/sn_uploads/quizzes/terrestrial_FR.jpg"),)
            ),
            const SizedBox(height: 20),
            Container( // Container for Questions
                width:350,
                height:150,
                padding: EdgeInsets.all(10),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(
                      width: 2.0,
                      color: _resp_correct == 0 ? Colors.blueGrey.shade200 : (_resp_correct == 1 ? Colors.green : Colors.red), )
                ),
                child:
                Text(_questions[_index].questionText.toString(),
                  style: const TextStyle(fontSize: 18),
                  textAlign: TextAlign.center,)
            ),
            const SizedBox(height: 40),
            Container( // Container for Buttons
              width:350,
              height:50,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(
                    onPressed: _isButtonDisabled ? null : _checkAnswerTrue,
                    child: const Text("Vrai", style: TextStyle(fontSize: 18)),
                  ),
                  ElevatedButton(
                      onPressed: _isButtonDisabled ? null : _checkAnswerFalse,
                      child: const Text("Faux", style: TextStyle(fontSize: 18))
                  ),
                  ElevatedButton(
                    onPressed: !_isButtonDisabled ? null : _nextQuestion,
                    child: Wrap(children:  [
                      Text(_quizIsOver() ? "Recommencer" :"Suivant", style: const TextStyle(fontSize: 18)),
                      Icon(Icons.keyboard_arrow_right)
                    ],
                    ),

                  ),

                ],),
            ),
            const SizedBox(height: 100),
          ],
        ),
      ),

    );
  }

}

class Question{
  String questionText;
  bool isCorrect;

  Question({required this.questionText, required this.isCorrect});

  bool valid(bool resp){
    return resp == isCorrect;
  }

}



/*import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Questions/ Reponses',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const QuizzPage(title: 'Quizz'),
    );
  }
}

class  QuizzPage extends StatefulWidget {
  const  QuizzPage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<QuizzPage> createState() => SomeQuizzPageState();
}
class Question {
  String questionText;
  bool isCorrect;
  Question({required this.questionText, required this.isCorrect});
}
class SomeQuizzPageState extends State<QuizzPage> {
  String questionPosee= "La France a dû ceder l'Alsace et la Moselle (une region de la Lorraine) à l'Allemagne apres la guerre de 1870-1871.";
  String reponse = "";
  int numQuestion= 0;
  List<Question> mes_Questions = <Question> [
    Question(isCorrect: false, questionText: ''),Question(isCorrect: false, questionText: ''),
    Question(isCorrect: true, questionText: ''),Question(isCorrect: false, questionText: ''),
    Question(isCorrect: false, questionText: ''),Question(isCorrect: true, questionText: ''),
    Question(isCorrect: true, questionText: ''),Question(isCorrect: false, questionText: ''),
    Question(isCorrect: false, questionText: ''),Question(isCorrect: true, questionText: ''),
    Question(isCorrect: true, questionText: ''),Question(isCorrect: false, questionText: '')
  ];
  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
          centerTitle: true,
          backgroundColor: Colors.black,
        ),
        backgroundColor: Colors.blueAccent,
        body: Column(

          children:<Widget>[

              Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 200,
                  width: 300,
                  decoration:  BoxDecoration(
                    image: DecorationImage(
                      image : NetworkImage("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcd94k8H_joukAVuyv1hpqoIvvgv_F6PNEfA&usqp=CAU"),
                        fit: BoxFit.fill
                  ),
                ),
              ),
              ),
            Align(
              alignment: Alignment.center,
              child: Container(
                alignment: Alignment.center,
                height: 140,
                width: 300,
                decoration: BoxDecoration(
                  border: Border.all(width: 1, color: Colors.white),
                  borderRadius: BorderRadius.circular(20)
                ),
                child: RichText(
                  text: TextSpan(
                    text: questionPosee,
                    style: TextStyle(
                      fontStyle: FontStyle.italic,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      height: 1.6

                    )
                  ),
                ),
              ),
            )
          ]
        ),
    );

  }

}

 */



